<?php

use Illuminate\Database\Seeder;
use App\Event;
class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Event::create(['title' => 'Test Event',
                    'location'  =>  '329',
                    'datetime'  =>  new \DateTime(),
                    'description' =>  'this is a test event description.'
                    ]);
    }
}
