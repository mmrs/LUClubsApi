<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      User::create(['name' => 'admin',
                    'password' => 'a',
                    'student_id'  =>  '00000',
                    'department'  =>  'admin',
                    'batch' =>  'admin',
                    'role'  =>  'admin',
                    'blood_group'   =>  'A+',
                    'photo_url'  =>  '0000000000',
                    'mobile'  => '0000000'
                    ]);

    User::create(['name' => 'test',
                    'password' => 'a',
                    'student_id'  =>  '00001',
                    'department'  =>  'CSE',
                    'batch' =>  '2012',
                    'role'  =>  'user',
                    'blood_group'   =>  'O+',
                    'email' =>  'test@mail.com',
                    'mobile'  =>  '1002313238',
                    'photo_url'  =>  '/profile/test.png',
                    'mobile'  => '11111111'
                    ]);
    }
}
