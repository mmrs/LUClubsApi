<?php

use Illuminate\Database\Seeder;
use App\Club;

class ClubsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Club::create(['name' => 'Test Club',
                    'photo_url'  =>  '/clubs/sust.png'
                    ]);

    }
}
