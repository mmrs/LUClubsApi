<?php

use Illuminate\Database\Seeder;
use App\UserClub;
class UserClubTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      UserClub::create(['user_id' =>  2,
                        'club_id' =>  1,
                        'role'    =>  'user',
                        'status'  =>  'approved'
                    ]);
    }
}
