<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserClubTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('user_club', function (Blueprint $table) {

          $table->increments('id');

          $table->integer('user_id')->unsigned();
          $table->integer('club_id')->unsigned();
          $table->string('role');
          $table->string('status');

          $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');
                
          $table->foreign('club_id')->references('id')->on('clubs')
                ->onDelete('cascade')->onUpdate('cascade');
                
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
