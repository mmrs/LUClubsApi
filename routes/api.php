<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

    /*
    * POST method
    * params :
            student_id@string,
            password@string

    * url : http://localhost:8000/api/login
    * return JSON (status[201 on success, 401 on failure],
                  message [success or error message],
                  user [user information])
    *
    */
Route::post('login', 'UserController@doLogin')->name('login');

/*
* POST method
* params :
        student_id@integer,
        name@string,
        email@string
        department@image,
        batch@string,
        mobile@string,
        password@string

* url : http://localhost:8000/api/register
* return JSON (status[201 on success, 401 on failure],
              message [success or error message],
              user [user information])
*
*/

Route::post('register', 'UserController@doRegister')->name('register');

/*
* POST method
* params : [all are optional]
        name@string,
        password@string,
        mobile@string,
        photo_url@photo


* url : http://localhost:8000/api/profileUpdate/{userId}
* return JSON (status[201 on success, 401 on failure],
              message [success or error message],
              user [user information])
*
*/

Route::post('profileUpdate/{id}', 'UserController@profileUpdate')->name('profileUpdate');

/*
* GET method
* params : user_id@integer
* url : http://localhost:8000/api/clubs/{userId}
* return JSON (status[201 on success, 401 on failure],
              message [success or error message],
              user [user information],
              clubs [clubs list with member or not info])
*
*/
Route::get('clubs/{user_id}', 'ClubsController@listClubs')->name('listClubs');

/*
* POST method
* params : [all are required]
        name@string,
        admin_id@integer,
        photo_url@photo

* url : http://localhost:8000/api/clubs/create
* return JSON (status[201 on success, 401 on failure],
              message [success or error message],
              club [club information])
*
*/
Route::post('clubs/create', 'ClubsController@createClub')->name('createClub');


/*
* GET method
* params :
* url : http://localhost:8000/api/events
* return JSON (status[201 on success, 401 on failure],
              message [success or error message],
              events [events list])
*
*/

Route::get('events', 'EventController@listEvents')->name('listEvents');


/*
* POST method
* params : [all are required]
        title@string,
        location@string,
        datetime@DateTime,
        description@text

* url : http://localhost:8000/api/events/create
* return JSON (status[201 on success, 401 on failure],
              message [success or error message],
              event [event information])
*
*/
Route::post('events/create', 'EventController@createEvent')->name('createEvent');

/*
* GET method
* params : club_id@integer
* url : http://localhost:8000/api/clubs/delete
* return JSON (status[201 on success, 401 on failure],
              message [success or error message],
              events [events list])
*
*/

Route::get('clubs/delete/{club_id}', 'ClubsController@deleteClub')->name('deleteClub');


/*
* POST method
* params : club_id@integer
*          user_id@integer
* url : http://localhost:8000/api/clubs/join
* return JSON (status[201 on success, 401 on failure],
              message [success or error message]
              )
*
*/

Route::post('clubs/join', 'ClubsController@joinClub')->name('joinClub');


/*
* GET method
* params : club_id@integer
* url : http://localhost:8000/api/clubs/members
* return JSON (status[201 on success, 401 on failure],
              message [success or error message],
              club [club info with list of members
                    with personal info]
              )
*
*/

Route::get('clubs/members/{club_id}', 'ClubsController@listMembers')->name('listMembers');



/*
* POST method
* params : club_id@integer
*          user_id@integer
*          status@string
* url : http://localhost:8000/api/clubs/processRequest
* return JSON (status[201 on success, 401 on failure],
              message [success or error message]
              )
*
*/

Route::post('clubs/processRequest', 'ClubsController@processRequest')->name('processRequest');


/*
* GET method
* params : club_id@integer
* url : http://localhost:8000/api/clubs/requests
* return JSON (status[201 on success, 401 on failure],
              message [success or error message],
              club [club info with list of requests
                    with personal info]
              )
*
*/

Route::get('clubs/requests/{club_id}', 'ClubsController@listRequests')->name('listRequests');

/*
* GET method
* params : club_id@integer
* url : http://localhost:8000/api/clubs/messages
* return JSON (status[201 on success, 401 on failure],
              message [success or error message],
              messages [message with user info]
              )
*
*/

Route::get('clubs/messages/{club_id}', 'MessagesController@messageList')->name('listMessages');


/*
* POST method
* params : club_id@integer
           user_id@integer
           message_body@string
* url : http://localhost:8000/api/clubs/messages/new
* return JSON (status[201 on success, 401 on failure],
              message [success or error message]
              )
*
*/

Route::post('clubs/messages/new', 'MessagesController@newMessage')->name('newMessages');



/*
* GET method
* params : club_id@integer
           user_id@integer
* url : http://localhost:8000/api/members/remove/{club_id}/{user_id}
* return JSON (status[201 on success, 401 on failure],
              message [success or error message]
              )
*
*/

Route::get('members/remove/{club_id}/{user_id}', 'ClubsController@removeMember')->name('removeMember');


/*
* GET method
* params : club_id@integer
           user_id@integer
           role@string
* url : http://localhost:8000/api/members/role/{club_id}/{user_id}
* return JSON (status[201 on success, 401 on failure],
              message [success or error message]
              )
*
*/

Route::get('members/role/{club_id}/{user_id}/{role}', 'ClubsController@updateRole')->name('updateRole');


/*
* GET method
* params : user_id@integer
* url : http://localhost:8000/api/password/recovery/{user_id}
* return JSON (status[201 on success, 401 on failure],
              message [success or error message]
              )
*
*/

Route::get('password/recovery/{email}', 'UserController@recoverPassword')->name('recoverPassword');
