<?php

namespace App;
use App\UserClub;
use App\Message;

use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
    public function members(){
    	return $this->hasMany('App\UserClub');
    }

    public function messages(){
    	return $this->hasMany('App\Message');
    }
}