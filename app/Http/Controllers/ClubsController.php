<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Club;
use App\User;
use App\UserClub;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class ClubsController extends Controller
{
    public function listClubs($user_id){
        try {
            $user = User::findOrFail($user_id);

            if($user->role == 'admin'){
                $clubs = Club::orderBy('name', 'ASC')->get();
            }else{
                $clubs = Club::orderBy('name', 'ASC')->get();
                //find all clubs that the user is in
                foreach ($clubs as $club) {
                    $club_user = UserClub::where('user_id',$user->id)
                                                  ->where('club_id',$club->id)
                                                  ->first();
                  if($club_user){
                    $club['user_club'] = $club_user;
                  }else{
                    $club['user_club'] = 'null';
                  }
                }
            }
            return Response::json([
              'message' => 'request success!',
              'user'  =>  $user,
              'clubs' =>  $clubs
            ], 201);

        } catch (Exception $e) {
            return Response::json([
              'message' => 'user not found!'
            ], 401);
        }

    }

    public function listMembers($club_id){
        try {

            $club = Club::where('id',$club_id)
                          ->with('members')
                          ->first();


            foreach ($club->members as $member) {
              $userClubs = UserClub::where('user_id', $member->user_id)
                                    ->where('club_id', '!=', $club->id)
                                    ->with('club')
                                    ->get();

//              $member['userInfo'] = User::find($member->user_id);
              $member['clubs'] = $userClubs;

            }

            if($club){
              return Response::json([
              'message' => 'request success!',
              'club' =>  $club
            ], 201);
            }else{
              throw new Exception();

            }


        } catch (Exception $e) {
            return Response::json([
              'message' => 'club not found!'
            ], 401);
        }

    }


    public function removeMember($club_id, $user_id){
        try {

            $membership = UserClub::where('club_id',$club_id)
                          ->where('user_id', $user_id)
                          ->first();

            if($membership){

              if($membership->delete()){
                return Response::json([
                'message' => 'member deleted successfully!'
                ], 201);
              }else{
                  return Response::json([
                  'message' => 'member deletion failed!'
                ], 401);
              }
            
            }else{
            
              return Response::json([
                'message' => 'memebership not found!'
              ], 401);  
            }
            
        } catch (Exception $e) {
            return Response::json([
              'message' => 'club not found!'
            ], 401);
        }

    }



    public function listRequests($club_id){
        try {

            $club = Club::where('id',$club_id)
                          ->with('members')
                          ->get();

            if($club){
              $requests = UserClub::where('club_id', $club_id)
                                    ->where('status','pending')
                                    ->orderBy('created_at', 'DESC')
                                    ->get();

              return Response::json([
              'message' => 'request success!',
              'requests' =>  $requests
            ], 201);
            }else{
              throw new Exception();

            }


        } catch (Exception $e) {
            return Response::json([
              'message' => 'club not found!'
            ], 401);
        }

    }

    public function createClub(Request $request)
    {
      try {


        $rules = array
        (
                    'name'      =>  'required',
                    'photo_url' =>  'image|required',
                    'admin_id'  =>  'required|exists:users,student_id'
        );
        $allInput = $request->all();
        $validation = Validator::make($allInput, $rules);

        // dd($allInput);

        if ($validation->fails())
        {

          return Response::json([
            'message' => 'validation failed!'
          ], 401);

        } else
        {

            $club = new Club();
            $club->name = $allInput['name'];
            $club->description = $allInput['description'];
            
            if(isset($allInput['photo_url'])) {

                $file = \Input::file('photo_url');
                $time = strtotime("now");
                $photo_url = '/clubs/'.$time.'.jpg';
                $image = Image::make($file);

                $image->resize(null, 200, function ($constraint) {
                    $constraint->aspectRatio();
                });

                $image->save(public_path().$photo_url);

                $club->photo_url = $photo_url;

              }

            if($club->save()){
              $user_club = new UserClub();
              $admin = User::where('student_id', $allInput['admin_id'])->first();
              $user_club->user_id = $admin->id;
              $user_club->club_id = $club->id;
              $user_club->role  = 'admin';
              $user_club->status = 'approved';

              if($user_club->save()){
                return Response::json([
                  'message' => 'club creation successful!',
                  'club'    =>  $club
                ], 201);
              }else{
                return Response::json([
                  'message' => 'club admin creation failed!'
                ], 401);
              }
            }else{
              return Response::json([
                'message' => 'club creation failed!'
              ], 401);
            }
        }
      } catch (Exception $e) {
        return Response::json([
          'message' => 'Error happened!'
        ], 401);
      }
    }

    public function deleteClub($club_id){
      try {

        $club = Club::findOrFail($club_id);

        if($club->delete()){
            return Response::json([
            'message' => 'club deleted successfully!'
          ], 201);
        }else{
            return Response::json([
            'message' => 'club deletion failed!'
          ], 401);
        }

      } catch (Exception $e) {
        return Response::json([
          'message' => 'request failed!'
        ], 401);
      }


    }

    public function joinClub(Request $request)
    {
      try {

        $rules = array
        (
                    'user_id'    =>  'required|exists:users,id',
                    'club_id'    =>  'required|exists:clubs,id'
        );

        $allInput = $request->all();
        $validation = Validator::make($allInput, $rules);

        if ($validation->fails())
        {

          return Response::json([
            'message' => 'validation failed!'
          ], 401);

        } else
        {
            $user_club = new UserClub();
            $user_club->user_id = $allInput['user_id'];
            $user_club->club_id = $allInput['club_id'];
            $user_club->role = 'user';
            $user_club->status = 'pending';

            if($user_club->save()){
              return Response::json([
                'message' => 'club join successful!'
              ], 201);

            }else{
              return Response::json([
                'message' => 'club join request failed!'
              ], 401);
            }
        }
      } catch (Exception $e) {
        return Response::json([
          'message' => 'Error happened!'
        ], 401);
      }
    }

    public function updateRole($club_id, $user_id, $role){
      try {

            $membership = UserClub::where('club_id',$club_id)
                          ->where('user_id', $user_id)
                          ->first();

            if($membership){

              $membership->role = $role;

              if($membership->save()){
                return Response::json([
                'message' => 'membership changed successfully!'
                ], 201);
              }else{
                  return Response::json([
                  'message' => 'membership change failed!'
                ], 401);
              }
            
            }else{
            
              return Response::json([
                'message' => 'memebership not found!'
              ], 401);  
            }
            
        } catch (Exception $e) {
            return Response::json([
              'message' => 'club not found!'
            ], 401);
        }
    }

    public function processRequest(Request $request)
    {
      try {

        $rules = array
        (
                    'user_id'    =>  'required|exists:users,id',
                    'club_id'    =>  'required|exists:clubs,id',
                    'status'     =>  'required'
        );

        $allInput = $request->all();
        $validation = Validator::make($allInput, $rules);

        if ($validation->fails())
        {

          return Response::json([
            'message' => 'validation failed!'
          ], 401);

        } else
        {
            $user_club = UserClub::where('user_id', $allInput['user_id'])
                                  ->where('club_id', $allInput['club_id'])
                                  ->first();
            if($user_club){
              $user_club->status = $allInput['status'];

              if($user_club->save()){
                return Response::json([
                  'message' => 'member request processed successful'
                ], 201);

              }else{
                return Response::json([
                  'message' => 'member request processed failed'
                ], 401);
              }
            }else{
              return Response::json([
                  'message' => 'member request not found'
                ], 401);
            }


        }
      } catch (Exception $e) {
        return Response::json([
          'message' => 'Error happened!'
        ], 401);
      }
    }
}
