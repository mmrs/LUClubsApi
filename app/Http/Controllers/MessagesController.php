<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Club;
use App\User;
use App\Message;
use App\UserClub;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Response;
use Illuminate\Support\Facades\Input;

class MessagesController extends Controller
{
    public function messageList($club_id){
    	try{

    		$club = Club::findOrFail($club_id);
    		$messages = Message::where('club_id', $club_id)
    							->with('user')
    							->orderBy('created_at', 'DESC')
    							->get();

        return Response::json([
          'message' => 'request success!',
          'messages' =>  $messages
        ], 201);

    	}catch(Exception $ex){
        return Response::json([
          'message' => 'user not found!'
        ], 401);
    	}
    }

    public function newMessage(Request $request)
    {
      try {

        $rules = array
        (
                    'user_id'           =>  'required|exists:users,id',
                    'club_id'           =>  'required|exists:clubs,id',
                    'message_body'      =>  'required'
        );

        $allInput = $request->all();
        $validation = Validator::make($allInput, $rules);

        if ($validation->fails())
        {

          return Response::json([
            'message' => 'validation failed!'
          ], 401);

        } else
        {
          $message = new Message();
          $message->user_id = $allInput['user_id'];
          $message->club_id = $allInput['club_id'];
          $message->message_body = $allInput['message_body'];

          if($message->save()){
            return Response::json([
              'message' => 'Message sent!'
            ], 201);
          }else{
            return Response::json([
              'message' => 'Message could not be sent!'
            ], 401);
          }
        }
      } catch (Exception $e) {
        return Response::json([
          'message' => 'Error happened!'
        ], 401);
      }
    }

}
