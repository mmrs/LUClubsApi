<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Response;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{

  public function recoverPassword($email){
    try{
      $user = User::where('email',$email)->first();
      
      if($user){

        $password = $user->password;
        $email = $user->email;

        //send recovery mail to the user email
        $response = \Mail::send('mail.passwordRecovery', 
        
        ['password' => $password],
        
        function ($message) use ($email)
        {
            $message->from('noreply@videoportal.com', 'Password Recovery');
            $message->to($email)->subject('Recover Password');
        });

        return Response::json([
                'message' => 'mail sent successfully!'
              ], 201);
      }else{
        return Response::json([
              'message' => 'user not found!'
            ], 401);  
      }

    }catch(Exception $ex){
      return Response::json([
              'message' => 'recovery failed!'
            ], 401);
    }
  }

  public function doLogin(Request $request)
  {
      $rules = array
      (
                  'student_id'    => 'required',
                  'password' => 'required'
      );
      $allInput = $request->all();
      $validation = Validator::make($allInput, $rules);

      // dd($allInput);


      if ($validation->fails())
      {

        return Response::json([
          'message' => 'validation failed!'
        ], 401);

      } else
      {

          $user = User::where('student_id', $allInput['student_id'])
                      ->where('password', $allInput['password'])
                      ->first();
          if($user){
            return Response::json([
              'message' => 'login successful!',
              'user'  =>  $user
            ], 201);
          }else{
            return Response::json([
              'message' => 'login failed!'
            ], 401);
          }
      }
  }

  public function doRegister(Request $request)
  {
      $rules = array
      (
                  'student_id'    => 'required',
                  'name'  =>  'required',
                  'email' =>  'email|required',
                  'department'  =>  'required',
                  'batch'  =>  'required',
                  'blood_group'  =>  'required',
                  'password' => 'required',
                  'mobile'  => 'required'
      );
      $allInput = $request->all();
      $validation = Validator::make($allInput, $rules);

      // dd($allInput);

      if ($validation->fails())
      {

        return Response::json([
          'message' => 'validation failed!'
        ], 401);

      } else
      {

          $user = new User();
          $user->name = $allInput['name'];
          $user->email = $allInput['email'];
          $user->student_id = $allInput['student_id'];
          $user->password = $allInput['password'];
          $user->department = $allInput['department'];
          $user->batch = $allInput['batch'];
          $user->blood_group = $allInput['blood_group'];
          $user->role = 'user';
          $user->mobile = $allInput['mobile'];
          $user->photo_url = '/profile/test.png';

          if($user->save()){
            return Response::json([
              'message' => 'registration successful!',
              'user'  =>  $user
            ], 201);
          }else{
            return Response::json([
              'message' => 'registration failed!'
            ], 401);
          }
      }
  }

  public function profileUpdate(Request $request, $id)
  {
      $rules = array
      (
      );
      $allInput = $request->all();
      $validation = Validator::make($allInput, $rules);

      // dd($allInput);

      if ($validation->fails())
      {

        return Response::json([
          'message' => 'validation failed!'
        ], 401);

      } else
      {
        try{

            $user = User::findOrFail($id);

            if(isset($allInput['name'])){
                $user->name = $allInput['name'];
            }

            if(isset($allInput['email'])){
                $user->email = $allInput['email'];
            }
            if(isset($allInput['password'])){
                $user->password = $allInput['password'];
            }

            if(isset($allInput['mobile'])){
                $user->mobile = $allInput['mobile'];
            }

            if(isset($allInput['blood_group'])){
                $user->blood_group = $allInput['blood_group'];
            }

            if(isset($allInput['photo_url'])) {
                //delete all related files

                if($user->photo_url != '/profile/test.png'){
                  if(file_exists(public_path($user->photo_url))){

                    unlink(public_path($user->photo_url));

                  }
                }


                $file = \Input::file('photo_url');
                $time = strtotime("now");
                $photo_url = '/profile/'.$user->id.$time.'.jpg';
                $file->move(public_path().'/profile/',$user->id.$time.'.jpg');
                $image = \Image::make(public_path().$photo_url);
                $image->resize(null, 200, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $user->photo_url = $photo_url;

              }

            if($user->save()){
              return Response::json([
                'message' => 'profile updated!',
                'user'  =>  $user
              ], 201);
            }else{
              return Response::json([
                'message' => 'update failed!'
              ], 401);
            }
          }catch(Exception $ex){
            return Response::json([
              'message' => 'user not found!'
            ], 401);
          }
      }
  }
}
