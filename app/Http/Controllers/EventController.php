<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Event;
use App\Club;
use App\User;
use App\UserClub;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class EventController extends Controller
{
    public function listEvents(){
      try {
        $curreent_datetime = date('Y-m-d H:i:s');
        $events = Event::where('datetime', '>', $curreent_datetime)
                        ->orderBy('datetime','ASC')
                        ->get();

        return Response::json([
          'message' => 'request success!',
          'events'  =>  $events
        ], 201);
      } catch (Exception $e) {
        return Response::json([
          'message' => 'request failed!'
        ], 401);
      }


    }

    public function createEvent(Request $request)
    {
      try {


        $rules = array
        (
                    'title'       =>  'required',
                    'location'    =>  'required',
                    'datetime'    =>  'required',
                    'description' =>  'required'
        );
        $allInput = $request->all();
        $validation = Validator::make($allInput, $rules);

        // dd($allInput);

        if ($validation->fails())
        {

          return Response::json([
            'message' => 'validation failed!'
          ], 401);

        } else
        {

            $event = new Event();
            $event->title = $allInput['title'];
            $event->location = $allInput['location'];
            $event->datetime = $allInput['datetime'];
            $event->description = $allInput['description'];

            if($event->save()){
                return Response::json([
                  'message' => 'event creation successful!',
                  'event'    =>  $event
                ], 201);

            }else{
              return Response::json([
                'message' => 'event creation failed!'
              ], 401);
            }
        }
      } catch (Exception $e) {
        return Response::json([
          'message' => 'Error happened!'
        ], 401);
      }
    }
}
