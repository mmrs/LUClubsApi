<?php

namespace App;
use App\Club;
use App\User;

use Illuminate\Database\Eloquent\Model;

class UserClub extends Model
{
    protected $table = 'user_club';
    protected $with = ['user'];

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function club(){
    	return $this->belongsTo('App\Club');
    }
}
