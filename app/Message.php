<?php

namespace App;
use App\User;
use App\Club;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function club(){
    	return $this->belongsTo('App\Club');
    }
}